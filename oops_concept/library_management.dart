import 'dart:io';

class Book {
  int? bookStudentId;
  String? bookName;
  int? id;

  String? authorName;
  int? rackNumber;
}

class Student {
  int? rollNumber;
  int? id;
  String? studentName;
}

void main() {
  List<Student> studentDetails = [];
  List<Book> bookDetail = [];
  List<Book> issueBooks = [];
  int bookId = 0;
  int studentId = 0;

  int? input;

  while (input != 7) {
    print("\n             LIBRARY MANAGEMENT SYSTEM\n");
    print("1. Add book Details");
    print('2. Show All Book Details');
    print("3. Filter Books By Author");
    print("4. Filter Books By Rack Number");
    print("5. Filter By Book Name");
    print('6. Add Student Details');
    print('7. Student Details');
    print('8. Taking Book');
    print('Available Book');
    print('10. Filter Books  by Student id(Student Hava a Books)');
    print('11. Exit');
    print("\n\nEnter Any One Option");
    int? input = int.parse(stdin.readLineSync()!);
    switch (input) {
      case 1:
        print('How Many Books Do You Want Add?');

        int howManyBooksYouAdd = int.parse(stdin.readLineSync()!);

        for (int i = 0; i < howManyBooksYouAdd; i++) {
          Book book = Book();

          print("Enter book name = ");
          String newBookName = stdin.readLineSync()!;
          book.bookName = newBookName;

          print("Enter author name = ");
          String? newAuthorName = stdin.readLineSync();
          book.authorName = newAuthorName;

          print("Enter rack number = ");
          int? newRackNumber = int.parse(stdin.readLineSync()!);
          book.rackNumber = newRackNumber;
          book.id = ++bookId;

          bookDetail.add(book);
        }

        break;
      case 2:
        if (bookDetail.isNotEmpty) {
          print('\nBookName   AuthorName    RackNumber    Id');
          // for (int i = 0; i < bookDetail.length; i++) {
          //   print('Book name: ${bookDetail[i].bookName}');

          //   print('Author Name: ${bookDetail[i].authorName}');
          //   print('Rack Number${bookDetail[i].rackNumber}');
          //   print("${bookDetail[i].book.student.rollNumber}");
          //   print("${bookDetail[i].book.student.studentName}");
          // }
          for (final Book bookInfo in bookDetail) {
            print('${bookInfo.bookName}'
                '          ${bookInfo.authorName}'
                '          ${bookInfo.rackNumber}'
                '          ${bookInfo.id}');
          }
        } else {
          print('please add book details');
          //return;
          break;
        }

        break;
      case 3:
        if (bookDetail.isNotEmpty) {
          print('\nEnter Author Name\n');
          String filterByAuthorName = stdin.readLineSync()!;
          print('\nBook Name   Author Name    Rack Number');
          for (int i = 0; i < bookDetail.length; i++) {
            if (filterByAuthorName.toLowerCase() ==
                bookDetail[i].authorName.toString().toLowerCase()) {
              print(' ${bookDetail[i].bookName}      '
                  '${bookDetail[i].authorName}      '
                  '    ${bookDetail[i].rackNumber}');
            }
          }
        } else {
          print('please add book details');
          break;
        }

        break;
      case 4:
        if (bookDetail.isNotEmpty) {
          print('Enter Rack Number');
          int filterByRackNumber = int.parse(stdin.readLineSync()!);
          print('\nBook Name   Author Name    Rack Number');

          for (int i = 0; i < bookDetail.length; i++) {
            if ((filterByRackNumber == bookDetail[i].rackNumber)) {
              print(' ${bookDetail[i].bookName}      '
                  '${bookDetail[i].authorName}      '
                  '    ${bookDetail[i].rackNumber}');
            }
          }
        } else {
          print('Please add book details');
          break;
        }
        break;
      case 5:
        {
          if (bookDetail.isNotEmpty) {
            print('\nEnter Book name');
            String? filterByBookName = stdin.readLineSync();
            print('Book name');

            for (int i = 0; i < studentDetails.length; i++) {
              if (bookDetail[i].bookName!.contains('${filterByBookName}')) {
                print(' ${bookDetail[i].bookName}');
              }
            }
          } else {
            print('Please add book details');
          }
        }

        break;
      case 6:
        print('How Many Student Add you');
        int? addStudent = int.parse(stdin.readLineSync()!);
        for (int i = 1; i <= addStudent; i++) {
          Student student = Student();

          print("Enter Student name = ");
          String newStudentName = stdin.readLineSync()!;
          student.studentName = newStudentName;

          print("Enter Roll Number = ");
          int newRollNumber = int.parse(stdin.readLineSync()!);
          // if (studentDetails.contains('$newRollNumber')) {
          //    print(
          //       'Already add student below this Roll Number. So kindly, Add Another Roll Number ');

          // } else {
          student.rollNumber = newRollNumber;

          //}

          student.id = ++studentId;

          studentDetails.add(student);
        }

        break;
      case 7:
        if (studentDetails.isNotEmpty) {
          print('\nStudentName       RollNumber    Id');
          for (final Student studentInfo in studentDetails) {
            print("${studentInfo.studentName}          "
                "${studentInfo.rollNumber}          "
                "${studentInfo.id}");
          }
        } else {
          print('please add student details');

          break;
        }
        break;

      case 8:
        if (bookDetail.isEmpty) {
          print('Please add the book detail');
        } else {
          print('Enter Book Name If Want You');

          int whichbookIdYouWant = int.parse(stdin.readLineSync()!);
          bookId = whichbookIdYouWant;

          print('Enter Student id');
          int? studentId = int.parse(stdin.readLineSync()!);
          Book takeBook = bookDetail.firstWhere((book) => book.id == bookId);

          takeBook.bookStudentId = bookId;
          bookId = studentId;

          issueBooks.add(takeBook);
          print(issueBooks);
        }

        break;

      case 9:
        print('\nBookName   AuthorName    RackNumber    Id');

        for (final Book bookInfo in bookDetail) {
          if (bookInfo.bookStudentId == null) {
            print('${bookInfo.bookName}'
                '          ${bookInfo.authorName}'
                '          ${bookInfo.rackNumber}'
                '          ${bookInfo.id}');
          }
        }
        break;
      case 10:
        if (issueBooks.isNotEmpty) {
          print('\nEnter Student Name\n');
          int enterStudentId = int.parse(stdin.readLineSync()!);
          print('\nBook Name   Author Name    Rack Number     Id');
          for (int i = 0; i < issueBooks.length; i++) {
            if (enterStudentId == issueBooks[i].bookStudentId) {
              print(' ${issueBooks[i].bookName}      '
                  '${issueBooks[i].authorName}      '
                  '      ${issueBooks[i].rackNumber}'
                  '${issueBooks[i].id}      ');
            }
          }
        } else {
          print('please add book details');
          break;
        }

        break;

      case 11:
        exit(0);
      default:
        {
          print('please enter above any one value');
        }
    }
  }
}
