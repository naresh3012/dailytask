class StudentInfo  {
  Mark? mark;
  String? name;
  int? rollNumber;
  int? std;
  StudentInfo({this.name, this.rollNumber, this.std, this.mark});
  void displayInfo() {
    print(
        '${name}            ${rollNumber}            ${std}       ${mark!.totalMarks()}      ${mark!.percentageMark()!.toStringAsFixed(1)}');
  }
}

class Mark {
  int? tamil;
  int? english;
  int? maths;
  int? science;
  int? social;
  Mark({
    this.tamil,
    this.english,
    this.maths,
    this.science,
    this.social,


    
  });
  int totalMarks() {
    final total = tamil! + english! + maths! + science! + social!;

    return total;
  }

  double? percentageMark() {
    final percentage = totalMarks() / 500 * 100;

    return percentage;
  }
}

void main(List<String> args) {
  StudentInfo student1 =  StudentInfo(
      name: "arul",
      rollNumber: 1,
      std: 10,
      mark: Mark(
        tamil: 79,
        english: 89,
        maths: 96,
        science: 88,
        social: 81,
      ));

  StudentInfo student2 = new StudentInfo(
    name: "mohan",
    rollNumber: 2,
    std: 9,
    mark: Mark(tamil: 99, english: 95, maths: 97, science: 89, social: 100),
  );

  StudentInfo student3 = new StudentInfo(
      name: "mouli",
      rollNumber: 3,
      std: 8,
      mark: Mark(tamil: 89, english: 98, maths: 91, science: 90, social: 96));

  StudentInfo student4 = new StudentInfo(
      name: "ssaro",
      rollNumber: 4,
      std: 7,
      mark: Mark(tamil: 98, english: 92, maths: 88, science: 89, social: 93));

  StudentInfo student5 = new StudentInfo(
      name: "gokul",
      rollNumber: 5,
      std: 6,
      mark: Mark(tamil: 94, english: 95, maths: 85, science: 89, social: 99));

  
  print('StudentName   RollNumber    Std  TotalMarks   Percentage');

  student1.displayInfo();
  student2.displayInfo();
  student3.displayInfo();
  student4.displayInfo();
  student5.displayInfo();
}
