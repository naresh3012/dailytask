import 'dart:io';

void main() {
  int i, j, lastIndex;

  List<int> list = [1, 2, 3, 4, 5, 6, 7];
  print(list);

  print('enter number');

  int number = int.parse(stdin.readLineSync()!);
  lastIndex = list[list.length - 1];

  for (i = 0; i < number; i++) {
    for (j = list.length - 1; j > 0; j--) {
      list[j] = list[j - 1];
    }

    list[0] = lastIndex;
    lastIndex = list[list.length - 1];
  }

  for (i = 0; i < list.length; i++) {
    print(list[i]);
  }
  print(list);
}
