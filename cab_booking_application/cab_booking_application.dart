import 'dart:io';
import 'models/booking.dart';
import 'models/cab.dart';

List<Cab> availableCabs = [];
List<Cab> listOfCabs = [];
String locations = 'ABCDEF';
List<Cab> nearestCab = [];

void main() {
  listOfCabs = createCabs();
  while (true) {
    print('\n                    ***  CAB BOOKING APPLICATION   ***\n');

    print('1. Book Your Ride');
    print('2. Display  Cab Details');
    print('3. Exit');

    int? valueFromUser = int.tryParse(stdin.readLineSync()!);
    switch (valueFromUser) {
      case 1:
        bookRide();
        break;
      case 2:
        displayCabDetail();
        break;
      case 3:
        exit(0);
      default:
        {
          print('please enter above any one value');
        }
    }
  }
}

void bookRide() {
  int? earningsPerTrip;
  String? pickUpPoint;
  String? dropPoint;
  int? PickUpTime;
  int? dropTime;
  print('\n                    ***   HAPPY JOURNEY    ***\n');

  print('Please Enter PickUp Point');
  String? enterPickPoint = stdin.readLineSync()!.toUpperCase();
  print('Please Enter Drop Point');
  String? enterDropPoint = stdin.readLineSync()!.toUpperCase();
  print('Please Enter PickUp Time ');
  int? enterPickUpTime = int.parse(stdin.readLineSync()!);

  /* If enter invalid values print warning */

  if (!(locations.contains(enterPickPoint) &&
      locations.contains(enterDropPoint) &&
      enterPickUpTime <= 24)) {
    print(' Please Enter Valid Pickup / Drop points / Pickup Time');
    return;
  }
  availableCab(enterPickUpTime, enterPickPoint);

  if (availableCabs.isNotEmpty) {
    late Cab cabs;

    for (Cab cab in availableCabs) {
      cab.pickUpPoint = enterPickPoint;
      cab.dropPoint = enterDropPoint;
      cab.cabPickUpTime = enterPickUpTime;
      int distanceCustomerToCab =
          ((locations.indexOf('${cab.currentPoint}') + 1) -
              (locations.indexOf('${cab.pickUpPoint}') + 1));

      cab.nearest = distanceCustomerToCab.abs();

      nearestCab.add(cab);
    }
    /* find nearest based on cab current point and pickup point  */
    nearestCab.sort((a, b) => a.nearest!.compareTo(b.nearest!));

    Cab cab = nearestCab.first;
    cab.pickUpPoint = enterPickPoint;
    cab.dropPoint = enterDropPoint;
    cab.cabPickUpTime = enterPickUpTime;

    /* find distance of pickup to drop point (travel distance) */
    int? distancePickUpToDrop = ((locations.indexOf('${cab.dropPoint}')) -
        (locations.indexOf('${cab.pickUpPoint}')));
    /* Earnings calculate */
    earningsPerTrip = ((distancePickUpToDrop.abs() * 15) - 5) * 10 + 100;
    print(earningsPerTrip);
    /*Drop time calculate */
    cab.cabDropTime = cab.cabPickUpTime! + distancePickUpToDrop.abs();
    /* Cab available time after complete trip  */
    cab.avilableCabTime = cab.cabDropTime;

    /*Cab available point after complete trip */
    cab.currentPoint = cab.dropPoint;

    dropPoint = cab.dropPoint;
    pickUpPoint = cab.pickUpPoint;
    PickUpTime = cab.cabPickUpTime;
    dropTime = cab.cabDropTime;
    cabs = cab;

    cabs.wallet = cabs.wallet! + earningsPerTrip;

    /* If cabs are at same point => allocate low earning cab */
    nearestCab.sort((a, b) => a.wallet!.compareTo(b.wallet!));

    /*Cab allocatiopn */

    print('\nCab ${cabs.cabId} is allocated...');
    print('\nEnjoy your ride :)');
    Booking bookingDetail = Booking(
        cabId: cabs.cabId,
        dropPoint: dropPoint,
        dropTime: dropTime,
        earingsPerTrip: earningsPerTrip,
        pickUpPoint: pickUpPoint,
        pickUpTime: PickUpTime);
    cabs.bookingDetails.add(bookingDetail);
  }
  /* If available cabs empty booking will rejected */
  else {
    print('Booking rejected');
  }
}

/* create cabs n */

List<Cab> createCabs() {
  for (int i = 1; i <= 4; i++) {
    Cab cab = Cab(
      bookingDetails: [],
      avilableCabTime: 0,
      cabId: i,
      currentPoint: 'A',
      wallet: 0,
    );
    listOfCabs.add(cab);
  }
  return listOfCabs;
}

/*create avilable cabs  */
void availableCab(
  int cabPickUpTime,
  String enterPickPoint,
) {
  availableCabs.clear();
  for (Cab cab in listOfCabs) {
    if (cab.avilableCabTime! <= cabPickUpTime &&
        (locations.indexOf('${cab.currentPoint}') + 1) -
                (locations.indexOf('${enterPickPoint}') + 1) <=
            (cabPickUpTime - cab.avilableCabTime!)) {
      availableCabs.add(cab);
    }
  }
}

/* Display cab details */
displayCabDetail() {
  print('\n                    ***   CAB DETAILS   ***\n');
  for (Cab cab in listOfCabs) {
    if (cab.bookingDetails.isNotEmpty) {
      print('\nCabId : ${cab.cabId} Total Earnings:${cab.wallet}');

      print(
          '\n| CabId | PickPoint | DropPoint | PickTime | DropTime | Wallet |');
      print('----------------------------------------------------------------');

      for (Booking booking in cab.bookingDetails) {
        print(
            "|   ${booking.cabId}   |    ${booking.pickUpPoint}      |    ${booking.dropPoint}      |  ${booking.pickUpTime}       |    ${booking.dropTime}    |  ${booking.earingsPerTrip}  |");
      }
      print('----------------------------------------------------------------');
    }
  }
}
