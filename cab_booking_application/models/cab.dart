import 'booking.dart';

class Cab {
  int? cabId;
  int? cabPickUpTime;
  int? cabDropTime;
  String? pickUpPoint;
  String? dropPoint;
  String? currentPoint;
  int? avilableCabTime;
  int? wallet;
  List<Booking> bookingDetails;
  int? nearest;

  Cab({
    required this.bookingDetails,
    this.cabId,
    this.avilableCabTime,
    this.wallet,
    this.currentPoint,
    this.cabDropTime,
    this.dropPoint,
  });
}
