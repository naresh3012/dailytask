
class Booking {
  int? cabId;
  String? pickUpPoint;
  String? dropPoint;
  int? pickUpTime;
  int? dropTime;
  int? earingsPerTrip;
  

  Booking(
      {this.cabId,
      this.dropPoint,
      this.dropTime,
      this.pickUpPoint,
      this.pickUpTime,
      this.earingsPerTrip});
}
